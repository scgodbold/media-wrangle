#!/usr/bin/env python3
import click

import configparser
import glob
import os
import os.path
import re

from collections.abc import Iterator, Sequence


def build_targets(path: str, recurse: bool, include_root: bool) -> Iterator[str]:
    if not recurse:
        yield path
        return

    # Include parent directory for cleaning, I dont think this will impact naming
    if include_root:
        yield path
    for t in glob.glob(f'{path}/*'):
        # skip files
        if os.path.isdir(t):
            yield(t)


def log(cmd: str, message: str):
    print(f'[{cmd}] {message}')


def run_clean(path: str, dry_run: bool, extensions: Iterator[str]):
    for extension in extensions:
        for f in glob.glob(f'{path}/*.{extension}'):
            _, filename = os.path.split(f)
            log('Clean', f'Removing {filename}')
            if not dry_run:
                os.remove(f)


def build_new_name(template: str, match: re.Match, title_file: Sequence[str]) -> str:
    episode = int(match.group('episode'))
    values = {
        'title': title_file[episode-1] if title_file is not None else match.group('title'),
        'season': match.group('season'),
        'episode': episode,
        'extension': match.group('extension')
    }
    try:
        if match.group('episode2') is None:
            values['episode2'] = ''
        else:
            values['episode2'] = match.group('episode2')
    except:
        # This may not always be important, just add it as an empty string for no impact
        values['episode2'] = ''
    return template.format(**values)


def run_rename(path: str, dry_run: bool, matcher: str, template: str, titles: Sequence[str]):
    pattern = re.compile(matcher)
    for f in glob.glob(f'{path}/*'):
        path, filename = os.path.split(f)
        match = pattern.match(filename)
        if match is None:
            log('RENAME - Miss', f)
            continue
        new_name = build_new_name(template, match, titles)
        log('RENAME', f'{filename} -> {new_name}')
        if not dry_run:
            os.rename(f, f'{path}/{new_name}')


def run_scan(path: str, matcher: str):
    pattern = re.compile(matcher)
    for f in glob.glob(f'{path}/**/*'):
        path, filename = os.path.split(f)
        match = pattern.match(filename)
        if match is None:
            log('SCAN', f'Match missed: {f}')


@click.group('media-wrangle', help='collection of tools to help keep my media files in order')
@click.option('--dry-run', is_flag=True, help='Indicates a no actual operation will be performed')
@click.option('--recurse', is_flag=True, help='Perform this task across all subdirectories of the given target')
@click.option('--config', type=str, help='path to a config file', default='./config.ini')
@click.pass_context
def cli(ctx, dry_run, recurse, config):
    ctx.ensure_object(dict)
    cp = configparser.ConfigParser()
    cp.read(config)
    ctx.obj['config'] = cp
    ctx.obj['DRY_RUN'] = dry_run
    ctx.obj['RECURSE'] = recurse


@cli.command(help='Clean target directory(ies), removing info and picture files, helpful if you want to ensure a rescan')
@click.argument('target')
@click.pass_context
def clean(ctx, target: str):
    recurse = ctx.obj.get('RECURSE', False)
    dry_run = ctx.obj.get('DRY_RUN', True)
    extensions = ctx.obj['config'].get('clean', 'extensions').split(',')
    for t in build_targets(target, recurse, True):
        run_clean(t, dry_run, extensions)


@cli.command(help='Bulk rename for TV shows')
@click.argument('target')
@click.argument('matcher')
@click.option('--title-file', type=str, help='An ordered list of season episode titles to use for naming, will not work for recurse mode')
@click.pass_context
def rename(ctx, target: str, matcher: str, title_file: str):
    recurse = ctx.obj.get('RECURSE', False)
    dry_run = ctx.obj.get('DRY_RUN', True)
    template = ctx.obj['config'].get('rename', 'name_template')
    if title_file is not None and recurse:
        print('[ERROR] Cannot use title file in recursive mode.')
        return
    titles = None
    if title_file is not None:
        with open(title_file, 'r') as f:
            titles = f.readlines()
            titles = [t.strip() for t in titles]
    for t in build_targets(target, recurse, False):
        run_rename(t, dry_run, matcher, template, titles)


@cli.command(help='Scan for file name inconsistencies')
@click.argument('target')
@click.pass_context
def scan(ctx, target: str):
    matcher = ctx.obj['config'].get('scan', 'show_scan_matcher')
    if matcher is None:
        print('[ERROR] No `show_scan_matcher` regex configured, one must be provided for scan to work')
        return
    run_scan(target, matcher)


if __name__ == '__main__':
    cli()
