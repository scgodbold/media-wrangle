# media-wrangle

A sample python script I wrote to help manage my media libray. Handles common operations such as bulk rename, clean unwanted files, and scan for file name mismatches

## Installation

There is no formal installation for this script, its kind of ad-hoc. To get going ensure you have python3 virtaul environments and run the following commands

```bash
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

At this point its ready to go.

## Usage

```
Usage: media-wrangle.py [OPTIONS] COMMAND [ARGS]...

  collection of tools to help keep my media files in order

Options:
  --dry-run      Indicates a no actual operation will be performed
  --recurse      Perform this task across all subdirectories of the given
                 target
  --config TEXT  path to a config file
  --help         Show this message and exit.

Commands:
  clean   Clean target directory(ies), removing info and picture files,...
  rename  Bulk rename for TV shows
  scan    Scan for file name inconsistencies
```

### Examples

**Rename some files**
```
./media-wrangle.py rename /my/path "Foo - S(?P<season>\d*)E(?P<episode>\d*)(?P<episode2>-E\d*)? - (?P<title>.*).(?P<extension>mkv)"
[RENAME] Foo - S01E01 - Hello World.mkv -> Hello world S01E01.mkv
```

**Clean some stuff**

```
./media-wrangle.py --recurse clean /path/to/stuff
[CLEAN] /path/to/stuff/somepictureidontwant.jpg
```

**Scan for mismatches**
```
./media-wrangle.py scan /path/to/stuff
[SCAN] Match Missed: /path/to/stuff/mal-formated-thing
```

## Config

To stop modifying the source section all the time a config file was setup. A sample is included, section information and specifics are included

### Section: scan

Used to configure the scan operation

**show_scan_matcher**: A regex pattern that all shows are expected to comply too, any file not matching this in the scan directory will be highlighted

### Section: rename

Used to configure the bulk rename operation

**name_template**: A templated string used to name the show, currently it supports variables `title`, `episode`, `season`, `extension`, `episode2`

Episode2 is a special case whereby a two part episode series were merged in most situations this is not used.

### Section: clean

Used to configure the clean operation

**extensions**: A coma seperated list of extensions to be purged
